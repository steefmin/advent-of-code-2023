# Day 7

```elixir
Mix.install([
  {:kino, ">= 0.0.0"}
])
```

## Helper code

<!-- livebook:{"reevaluate_automatically":true} -->

```elixir
defmodule Helper do
  def run("part 1", test_input, answer, final_input) do
    do_run("part 1", test_input, answer, final_input)
  end

  def run("part 2", test_input, answer, final_input) do
    do_run("part 2", test_input, answer, final_input)
  end

  defp do_run(function_name, test_input, expected, final_input) do
    actual =
      test_input
      |> get_answer(function_name)
      |> return_as_string()

    IO.puts("Testing #{function_name}" |> green())
    Test.test(expected, actual)
    IO.puts("Test for #{function_name} passed!\n" |> green())

    IO.puts("Answer for #{function_name}:" |> green())

    final_input
    |> get_answer(function_name)
    |> return_as_string()
    |> IO.inspect()

    IO.puts("")

    :ok
  end

  defp read_input(input) do
    input
    |> Kino.Input.read()
  end

  defp get_answer(input, function_name) do
    arg =
      input
      |> read_input()

    method =
      function_name
      |> String.replace(" ", "")
      |> String.to_existing_atom()

    apply(Solution, method, [arg])
  end

  defp return_as_string(value) do
    cond do
      value |> is_integer() -> value |> Integer.to_string()
      value |> is_binary() -> value
      true -> value
    end
  end

  def green(text) do
    IO.ANSI.green() <> text <> IO.ANSI.reset()
  end
end

defmodule Test do
  import ExUnit.Assertions

  def test(expected, actual) do
    assert expected == actual
  end
end
```

## Input data

```elixir
default = """
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
"""

test_input = Kino.Input.textarea("test input", default: default)
```

```elixir
puzzle_input = Kino.Input.textarea("puzzle input")
```

## Solution

<!-- livebook:{"reevaluate_automatically":true} -->

```elixir
defmodule Solution do
  @types ["A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"]
  @types_with_j_rule ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"]

  def part1(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&handle_line/1)
    |> Enum.map(&score_hand/1)
    |> Enum.sort(&sort_hands(@types, &1, &2))
    |> total_winnings()
  end

  def total_winnings(list) do
    list
    |> Enum.reverse()
    |> Enum.with_index(1)
    |> Enum.map(fn {hand, rank} -> rank * hand.bid end)
    |> Enum.sum()
  end

  def sort_hands(types, [a | a_rest], [a | b_rest]) do
    sort_hands(types, a_rest, b_rest)
  end

  def sort_hands(types, [a | _], [b | _]) do
    get_value_for_card(types, a) > get_value_for_card(types, b)
  end

  def sort_hands(_types, a, b) when a.score > b.score, do: true
  def sort_hands(_types, a, b) when a.score < b.score, do: false

  def sort_hands(types, a, b) do
    sort_hands(types, a.hand, b.hand)
  end

  def determine_score_for_hand([5]), do: {7, :five_of_a_kind}
  def determine_score_for_hand([4, 1]), do: {6, :four_of_a_kind}
  def determine_score_for_hand([3, 2]), do: {5, :full_house}
  def determine_score_for_hand([3, 1, 1]), do: {4, :three_of_a_kind}
  def determine_score_for_hand([2, 2, 1]), do: {3, :two_pair}
  def determine_score_for_hand([2, 1, 1, 1]), do: {2, :one_pair}
  def determine_score_for_hand([1, 1, 1, 1, 1]), do: {1, :high_card}

  def score_hand(%{hand: hand} = result) do
    freqs =
      hand
      |> Enum.frequencies()

    dist =
      freqs
      |> Map.values()
      |> Enum.sort(:desc)

    {score, description} = determine_score_for_hand(dist)

    result
    |> Map.put_new(:score, score)
    |> Map.put_new(:description, description)
    |> Map.put_new(:freqs, freqs)
  end

  def handle_line(line) do
    [hand, bid] =
      line
      |> String.split(" ", trim: true)

    {bid, ""} = Integer.parse(bid)

    %{
      hand: hand |> String.split("", trim: true),
      bid: bid
    }
  end

  def get_value_for_card(types, type) do
    types
    |> Enum.reverse()
    |> Enum.with_index()
    |> Enum.into(%{})
    |> Map.get(type)
  end

  def part2(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&handle_line/1)
    |> Enum.map(&score_hand_with_j_rule/1)
    |> Enum.sort(&sort_hands(@types_with_j_rule, &1, &2))
    |> total_winnings()
  end

  def score_hand_with_j_rule(%{hand: hand} = result) do
    js =
      hand
      |> Enum.filter(&is_j?/1)
      |> Enum.count()

    freqs =
      hand
      |> Enum.reject(&is_j?/1)
      |> Enum.frequencies()

    dist =
      if js == 5 do
        [5]
      else
        [highest | rest] =
          freqs
          |> Map.values()
          |> Enum.sort(:desc)

        [highest + js | rest]
      end

    {score, description} = determine_score_for_hand(dist)

    result
    |> Map.put_new(:score, score)
    |> Map.put_new(:description, description)
    |> Map.put_new(:freqs, freqs)
  end

  def is_j?("J"), do: true
  def is_j?(_), do: false
end

Helper.run("part 1", test_input, "6440", puzzle_input)
Helper.run("part 2", test_input, "5905", puzzle_input)
```
